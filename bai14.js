class Product{
  constructor(id, name, categoryId, saleDate, qulyity, isdelete) {
      this.id = id;
      this.name = name;
      this.categoryId = categoryId;
      this.saleDate = saleDate;
      this.qulyity = qulyity;
      this.isdelete = isdelete;
  }
}
function listProducts(){
  const pushlistproduct = [];
  let product1 = new Product(1, 'CPU', 1, new Date(2020,12,21), 20, true);
  pushlistproduct.push(product1);
  let product2 = new Product(2, 'RAM', 2, new Date(2020,02,11), 12, true);
  pushlistproduct.push(product2);
  let product3 = new Product(3, 'HDD', 2, new Date(2020,05,05), 10, true);
  pushlistproduct.push(product3);
  let product4 = new Product(4, 'MAINX', 3, new Date(2020,05,08), 11, false);
  pushlistproduct.push(product4);
  let product5 = new Product(5, 'HEYLA', 1, new Date(2020,09,10), 32, false);
  pushlistproduct.push(product5);
  let product6 = new Product(6, 'KEYBOARD', 2, new Date(2020,11,26), 30, true);
  pushlistproduct.push(product6);
  let product7 = new Product(7, 'MOUSE', 2, new Date(2020,04,15), 43, false);
  pushlistproduct.push(product7);
  let product8 = new Product(8, 'VGA ', 3, new Date(2020,07,18), 15, true);
  pushlistproduct.push(product8);
  let product9 = new Product(9, 'Monitor', 1, new Date(2022,3,28), 26, true);
  pushlistproduct.push(product9);
  let product10 = new Product(10, 'TTK', 2, new Date(2022,3,28), 9, false);
  pushlistproduct.push(product10);
  return pushlistproduct;
}
const listProduct = listProducts();
// dùng reduce
function totalProductReduce(listProduct){
  const listfilterproduct = listProduct.filter(product => product.qulyity > 0 && product.isdelete === false);
  const listreduceproduct = listfilterproduct.reduce((total, listfilterproduct) => total += listfilterproduct.qulyity, 0);
  return listreduceproduct;
}

console.log(totalProductReduce(listProduct));
// không reduce
function totalProductNoReduce(listProduct){
  let total = 0;
  const listfilterproduct = listProduct.filter(product => product.qulyity > 0 && product.isdelete === false);
  for (producttotal of listfilterproduct){
      total += producttotal.qulyity;
  }
  return total;
}
console.log(totalProductNoReduce(listProduct));